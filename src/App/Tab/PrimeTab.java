/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package App.Tab;

import App.Models.Prime;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author EL OMRI
 */
public class PrimeTab extends AbstractTableModel{
    String[] titre = new String[]{"","Id", "Type", "Montant", "Date", "Employe", "Action"};
    Vector<Prime> vpr = new Vector<Prime>();
    
    @Override
    public int getRowCount() {
        return vpr.size();
    }

    @Override
    public int getColumnCount() {
        return titre.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Prime rs = vpr.get(rowIndex);
        switch(columnIndex){
            case 0 : return rs.isC();
            case 1 : return rs.getIdPrime();
            case 2 : return rs.getType();
            case 3 : return rs.getMontantprime();
            case 4 : return rs.getDateprime();
            case 5 : return rs.getEmploye();
            case 6 : return "Modifier";
            default:return null;
        }
    }
        
    public String getColumnName(int column){
	return titre[column];
    }
    
    public void setdata (Vector<Prime> etu){
        vpr = new Vector<Prime>();
	for(Prime p:etu){
		vpr.add(p);
	}
	fireTableChanged(null);
    }
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
      return column == 0;
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
         //To change body of generated methods, choose Tools | Templates.
        Prime ce = vpr.get(row);
        if (o instanceof Boolean && col == 0) 
            ce.setC((boolean)o);
        fireTableCellUpdated(row, col);
    }
    
    
}
