/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package App.Models;

import java.util.Date;

/**
 *
 * @author Geek-Dev
 */
public class Paiement {
    private  int idPaiement;
    private Date DatePaiement;
    private float Montant;
    private Employe employe;

    public Paiement(int idPaiement, Date DatePaiement, float Montant, Employe employe) {
        this.idPaiement = idPaiement;
        this.DatePaiement = DatePaiement;
        this.Montant = Montant;
        this.employe = employe;
    }

    public Paiement(int idPaiement, Date DatePaiement, float Montant) {
        this.idPaiement = idPaiement;
        this.DatePaiement = DatePaiement;
        this.Montant = Montant;
    }

   

    public Paiement() {
    }

    public int getIdPaiement() {
        return idPaiement;
    }

    public Date getDatePaiement() {
        return DatePaiement;
    }

    public float getMontant() {
        return Montant;
    }

    public void setIdPaiement(int idPaiement) {
        this.idPaiement = idPaiement;
    }

    public void setDatePaiement(Date DatePaiement) {
        this.DatePaiement = DatePaiement;
    }

    public void setMontant(float Montant) {
        this.Montant = Montant;
    }

    public Employe getIdEmploye() {
        return employe;
    }

    public void setIdEmploye(Employe idEmploye) {
        this.employe = idEmploye;
    }

    @Override
    public String toString() {
        return "Paiement{" + "idPaiement=" + idPaiement + ", DatePaiement=" + DatePaiement + ", Montant=" + Montant + ", employe=" + employe + '}';
    }
    
    
  
    
    
}
