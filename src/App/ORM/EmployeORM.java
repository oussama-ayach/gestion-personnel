/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.ORM;

import App.Models.Categorie;
import App.Models.Employe;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author taik
 */
public class EmployeORM extends ORM<Employe> {

    public EmployeORM() {  }
    
    @Override
    public Employe find(int id) {
        Employe em= new Employe();
        try {
            PreparedStatement ps = con.prepareStatement("select * from employe where idEmploye=?");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                em.setIdEmploye(rs.getInt(1));
                em.setNom(rs.getString(2));
                em.setPrenom(rs.getString(3));
                em.setDateNaissance(new Date(rs.getDate(4).getTime()));
                em.setDateEntree(new Date(rs.getDate(5).getTime()));
                em.setDateSortie(new Date(rs.getDate(6).getTime()));
                em.setCategorie(new Categorie());
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return em;
    }
    
    @Override
    public Vector<Employe> list() {
        Vector<Employe> vvl = new Vector<Employe>();
        try {
            PreparedStatement ps = con.prepareStatement("select e.*,c.* from employe as e left join categorie c on c.idCategorie=e.idCategorie");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Employe em = new Employe();
                em.setIdEmploye(rs.getInt(1));
                em.setNom(rs.getString(2));
                em.setPrenom(rs.getString(3));
                em.setDateNaissance(new Date(rs.getDate(4).getTime()));
                em.setDateEntree(new Date(rs.getDate(5).getTime()));
                em.setDateSortie(new Date(rs.getDate(6).getTime()));
                Categorie ct = new Categorie();
                ct.setIdCategotrie(rs.getInt(7));
                ct.setIntituleCategorie(rs.getString("IntituleCategorie"));
                ct.setJourConge(rs.getInt("NbJoursFerie"));
                ct.setPenalite(rs.getFloat("Penalite"));
                ct.setSalaire(rs.getFloat("Salaire"));
                em.setCategorie(ct);
                vvl.add(em);
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vvl;
    }

    @Override
    public Employe create(Employe obj) {
        try {
            PreparedStatement ps = con.prepareStatement("insert into employe(Nom,Prenom,DateNaissance,DateEntree,DateSortie,idCategorie) values(?,?,?,?,?,?)");
            ps.setString(1, obj.getNom());
            ps.setString(2, obj.getPrenom());
            ps.setDate(3, new Date(obj.getDateNaissance().getTime()));
            ps.setDate(4, new Date(obj.getDateEntree().getTime()));
            ps.setDate(5, new Date(obj.getDateSortie().getTime()));
            ps.setInt(6, obj.getCategorie().getIdCategotrie());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }
    
    
    @Override
    public Employe update(Employe obj) {
        try {
            PreparedStatement ps = con.prepareStatement("update employe set Nom=?,Prenom=?,DateNaissance=?,DateEntree=?,DateSortie=?,idCategorie=? where idEmploye=?");
            ps.setString(1, obj.getNom());
            ps.setString(2, obj.getPrenom());
            ps.setDate(3, new Date(obj.getDateNaissance().getTime()));
            ps.setDate(4, new Date(obj.getDateEntree().getTime()));
            ps.setDate(5, new Date(obj.getDateSortie().getTime()));
            ps.setInt(6, obj.getCategorie().getIdCategotrie());
            ps.setInt(7, obj.getIdEmploye());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public void delete(int id) {
        try {

            PreparedStatement ps = con.prepareStatement("delete from employe where idEmploye=?");
            ps.setInt(1, id);
            int nb = ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }

    
}
