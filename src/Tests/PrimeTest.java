/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import App.Models.Categorie;
import App.Models.Employe;
import App.Models.Prime;
import App.ORM.EmployeORM;
import App.ORM.PrimeORM;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.Vector;

/**
 *
 * @author Naima
 */
public class PrimeTest {

    public PrimeTest() {
    }
    
    public void runTests(){
        System.out.println("TESTS PRIME");
        System.out.println("==============================");
        System.out.println("App.ORM.PrimeORM => Find();");
        
        PrimeORM prorm = new PrimeORM();
        Prime pr = new Prime();
        Vector<Prime> vpr = new Vector<Prime>();
        pr=prorm.find(1);
        vpr = prorm.list();
        System.out.println("Emp : id :"+pr.getIdPrime()
                            +"Nom employe : "+pr.getEmploye().getNom());
        System.out.println("\nlist\n");
        for (int i = 0; i < vpr.size(); i++) {
                Prime prv = vpr.get(i);
                System.out.println("id :"+prv.getIdPrime()
                            +"   Nom employe : "+prv.getEmploye().getNom());
                
        }
        System.out.println("\n");
        EmployeORM emrom = new EmployeORM();
        Employe emp = emrom.find(2);
        //Prime p = new Prime(11,"men 7e99o", 1800, new Date(2015, 11, 10),emp);
        //prorm.delete(11);
        vpr = prorm.list();
        System.out.println("apres ajout");
        for (int i = 0; i < vpr.size(); i++) {
                Prime prv = vpr.get(i);
                System.out.println("id :"+prv.getIdPrime()+"   Nom employe : "+prv.getEmploye().getNom());
        }
    }
}
