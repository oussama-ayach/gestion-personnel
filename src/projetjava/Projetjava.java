/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava;

import App.GUI.MainGUI;
import System.Bootstrap;


/**
 *
 * @author taik
 */
public class Projetjava {

    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) {
        
        /**
         * Load Core Application
         */
        Bootstrap app = Bootstrap.getInstance();
        app.initDatabase(); // Set Default DB Connection
        
        /**
         * Example GUI
         */
        /*App.GUI.TestJFrame f = (App.GUI.TestJFrame) app.run("App.GUI.TestJFrame");
        f.setVisible(true);
        
        /**
         * Example Model
         */
        /*Tests.EmployeTest m = (Tests.EmployeTest) app.run("Tests.EmployeTest");
        m.runTests();
        
        Tests.CategorieTest c = (Tests.CategorieTest) app.run("Tests.CategorieTest");
        c.runTests();
        
        Tests.PrimeTest p = (Tests.PrimeTest) app.run("Tests.PrimeTest");
        p.runTests();*/
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainGUI().setVisible(true);
            }
        });
    }

}
