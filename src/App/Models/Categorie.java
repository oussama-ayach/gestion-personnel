/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Models;

/**
 *
 * @author NAIMA
 */
public class Categorie {
    
    private int IdCategotrie;
    private float salaire;
    private int jourConge;
    private String intituleCategorie;
    private float penalite;

    public Categorie() {
    }
    
    public Categorie(int IdCategotrie, float salaire, int jourConge, String intituleCategorie, float penalite) {
        this.IdCategotrie = IdCategotrie;
        this.salaire = salaire;
        this.jourConge = jourConge;
        this.intituleCategorie = intituleCategorie;
        this.penalite = penalite;
    }
    
    public Categorie(float salaire, int jourConge, String intituleCategorie, float penalite) {
        this.salaire = salaire;
        this.jourConge = jourConge;
        this.intituleCategorie = intituleCategorie;
        this.penalite = penalite;
    }

    public int getIdCategotrie() {
        return IdCategotrie;
    }

    public void setIdCategotrie(int IdCategotrie) {
        this.IdCategotrie = IdCategotrie;
    }

    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    public int getJourConge() {
        return jourConge;
    }

    public void setJourConge(int jourConge) {
        this.jourConge = jourConge;
    }

    public String getIntituleCategorie() {
        return intituleCategorie;
    }

    public void setIntituleCategorie(String intituleCategorie) {
        this.intituleCategorie = intituleCategorie;
    }

    public float getPenalite() {
        return penalite;
    }

    public void setPenalite(float penalite) {
        this.penalite = penalite;
    }

    /*@Override
    public String toString() {
        return "Categorie{" + "IdCategotrie=" + IdCategotrie + ", salaire=" + salaire + ", jourConge=" + jourConge + ", intituleCategorie=" + intituleCategorie + ", penalite=" + penalite + '}';
    }*/
    @Override
    public String toString() {
        return intituleCategorie;
    }
}
