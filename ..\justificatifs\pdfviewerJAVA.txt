import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
 
import javax.swing.*;
 
import com.sun.pdfview.*;
 
public class PDFView
{
	private PagePanel panel;
	private PDFFile pdffile;
	private int currentPage = 1;
 
	public PDFView() throws IOException
	{
		// set up the frame and panel
		JFrame frame = new JFrame("PDF Viewer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		panel = new PagePanel();
 
		frame.add(panel,BorderLayout.CENTER);
		JButton previousButton = new JButton("Précédent");
		previousButton.addActionListener(new ActionListener(){
 
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				goToPage(--currentPage);
			}
 
		});
		JButton nextButton = new JButton("Suivant");
		nextButton.addActionListener(new ActionListener(){
 
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				goToPage(++currentPage);
			}
 
		});
 
		JPanel southPanel = new JPanel(new GridLayout(1,2));
		southPanel.add(previousButton);
		southPanel.add(nextButton);
		frame.add(southPanel,BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);
 
		// load a pdf from a byte buffer
		File file = new File("C:/BOU_AFM-000257298.pdf");
		RandomAccessFile raf = new RandomAccessFile(file, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel
				.size());
		pdffile = new PDFFile(buf);
 
		// show the first page
		goToPage(currentPage);
	}
 
	public void loadPdf(File file) throws IOException
	{
		// load a pdf from a byte buffer
		RandomAccessFile raf = new RandomAccessFile(file, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel
				.size());
		pdffile = new PDFFile(buf);
 
		// show the first page
		PDFPage page2 = pdffile.getPage(currentPage);
		panel.showPage(page2);
	}
 
	public void goToPage(int nbpage) throws IndexOutOfBoundsException
	{
		if(nbpage<1||nbpage>pdffile.getNumPages())
			throw new IndexOutOfBoundsException("Invalid index requested :"+nbpage);
		PDFPage page = pdffile.getPage(nbpage);
		panel.showPage(page);
	}
 
	public static void main(final String[] args)
	{
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				try
				{
					PDFView view =new PDFView();
					view.loadPdf(new File("C:/BOU_AFM-000257298.pdf"));
 
				} catch (IOException ex)
				{
					ex.printStackTrace();
				}
			}
		});
	}
 
}