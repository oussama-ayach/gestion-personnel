/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Models;

import java.util.Date;

/**
 *
 * @author Naima
 */
public class Prime {
    boolean c;
    int idPrime;
    String Type;
    float Montantprime;
    Date Dateprime;
    Employe employe;

    public Prime() {
    }

    public Prime(String Type, float Montantprime, Date Dateprime, Employe employe) {
        this.Type = Type;
        this.Montantprime = Montantprime;
        this.Dateprime = Dateprime;
        this.employe = employe;
    }

    public Prime(int idPrime, String Type, float Montantprime, Date Dateprime, Employe employe) {
        this.idPrime = idPrime;
        this.Type = Type;
        this.Montantprime = Montantprime;
        this.Dateprime = Dateprime;
        this.employe = employe;
    }

    public boolean isC() {
        return c;
    }

    public void setC(boolean c) {
        this.c = c;
    }

    public int getIdPrime() {
        return idPrime;
    }

    public void setIdPrime(int idPrime) {
        this.idPrime = idPrime;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public float getMontantprime() {
        return Montantprime;
    }

    public void setMontantprime(float Montantprime) {
        this.Montantprime = Montantprime;
    }

    public Date getDateprime() {
        return Dateprime;
    }

    public void setDateprime(Date Dateprime) {
        this.Dateprime = Dateprime;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    @Override
    public String toString() {
        return "Prime{" + "idPrime=" + idPrime + ", Type=" + Type + ", Montantprime=" + Montantprime + ", Dateprime=" + Dateprime + ", employe=" + employe + '}';
    }
}