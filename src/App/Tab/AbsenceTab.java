/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.Tab;

import App.Models.Absence;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author masta
 */
public class AbsenceTab extends AbstractTableModel{
    
    String[] titre = new String[]{"","Id", "Debut", "Fin", "Motif", "Justificatif", "Employé","Action"};
    
    Vector<Absence> vab = new Vector<Absence>();

    @Override
    public int getRowCount() {
        return vab.size();
    }

    @Override
    public int getColumnCount() {
        return titre.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Absence ab = vab.get(rowIndex);
        switch(columnIndex){
            case 0 : return ab.isC();
            case 1 : return ab.getidAbsence();
            case 2 : return ab.getDebut();
            case 3 : return ab.getFin();
            case 4 : return ab.getMotif();
            case 5 : return ab.getJustificatif();
            case 6 : return ab.getEmploye();
            case 7 : return "Modifier";
            default:return null;
        }
    }
    
    public String getColumnName(int column){
	return titre[column];
    }
    
    public void setdata (Vector<Absence> etu){
        vab = new Vector<Absence>();
	for(Absence a:etu){
		vab.add(a);
	}
	fireTableChanged(null);
    }
    
    @Override
    public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
      return column == 0;
    }

    @Override
    public void setValueAt(Object o, int row, int col) {
         //To change body of generated methods, choose Tools | Templates.
        Absence ce = vab.get(row);
        if (o instanceof Boolean && col == 0) 
            ce.setC((boolean)o);
        fireTableCellUpdated(row, col);
    }
    
}
