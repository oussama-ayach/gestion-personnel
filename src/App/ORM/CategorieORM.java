/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App.ORM;

import App.Models.Categorie;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Naima
 */
public class CategorieORM extends ORM<Categorie>{

    @Override
    public Categorie find(int id) {
        Categorie co= new Categorie();
        try {
            PreparedStatement ps = con.prepareStatement("SELECT * FROM categorie WHERE idCategorie=?");
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            
            if (rs.next()) {
                co.setIdCategotrie(rs.getInt(1));
                co.setPenalite(rs.getFloat(2));
                co.setJourConge(rs.getInt(3));
                co.setSalaire(rs.getFloat(4));
                co.setIntituleCategorie(rs.getString(5));
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return co;
    }

    @Override
    public Vector<Categorie> list() {
        Vector<Categorie> vvl = new Vector<Categorie>();
        try {
            PreparedStatement ps = con.prepareStatement("select * from categorie");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Categorie co = new Categorie();
                co.setIdCategotrie(rs.getInt(1));
                co.setPenalite(rs.getFloat(2));
                co.setJourConge(rs.getInt(3));
                co.setSalaire(rs.getFloat(4));
                co.setIntituleCategorie(rs.getString(5));
                vvl.add(co);
            }
            ps.close();
        } catch (Exception ex) {
            Logger.getLogger(EmployeORM.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vvl;
    }

    @Override
    public Categorie create(Categorie obj) {
        try {
            PreparedStatement ps = con.prepareStatement("INSERT INTO categorie(Penalite,jourConge,Salaire,IntituleCategorie) VALUES(?,?,?,?)");
            ps.setFloat(1, obj.getPenalite());
            ps.setInt(2, obj.getJourConge());
            ps.setFloat(3, obj.getSalaire());
            ps.setString(4, obj.getIntituleCategorie());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public Categorie update(Categorie obj) {
        try {
            PreparedStatement ps = con.prepareStatement("UPDATE categorie SET Penalite=?,jourConge=?,Salaire=?,IntituleCategorie=? where idCategorie=?");
            ps.setFloat(1, obj.getPenalite());
            ps.setInt(2, obj.getJourConge());
            ps.setFloat(3, obj.getSalaire());
            ps.setString(4, obj.getIntituleCategorie());
            ps.setInt(5, obj.getIdCategotrie());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
        return null;
    }

    @Override
    public void delete(int id) {
        try {

            PreparedStatement ps = con.prepareStatement("DELETE FROM categorie where idCategorie=?");
            ps.setInt(1, id);
            int nb = ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
        }
    }
    
}
